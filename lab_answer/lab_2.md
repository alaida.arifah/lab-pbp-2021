1. Apakah perbedaan antara JSON dan XML?
Json:
 - Berdasar kepada bahasa pemrograman JavaScript.
 - Tidak memakai end tag.
 - Objek pada JSON memiliki tipe data berupa String, number, array, atau boolean.
 - Lebih mudah dibaca oleh manusia.
 - Hanya mendukung UTF08 encoding
XML :
 - Merupakan markup language.
 - Memakai start dan end tag.
 - Data di XML tidak memiliki tipe data.
 - Sulit untuk dibaca dan dinterpretasi oleh manusia.
 - Mendukung berbagai format encoding.

2. Apakah perbedaan antara HTML dan XML?
HTML :
 - HTML menampilkan data dan mendeskripsikan struktur dari sebuah laman web.
 - Tidak case sensitive.
 - Merupakan markup language.
 - Static.
XML  :
 - XML stores dan exchange data.
 - Case sensitive.
 - Merupakan markup language yang standar dan menjadi pendefinisi markup language lainnya.
 - Dynamic.