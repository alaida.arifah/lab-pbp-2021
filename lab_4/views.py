from django.shortcuts import render
from django.http.response import HttpResponseRedirect
from .models import Note
from .forms import NoteForm

# Create your views here.

def index(request):
    notes = Note.objects.all()
    response = {'notes' : notes}
    return render (request, 'lab4_index.html', response)

def add_note(request):
    
    form = NoteForm()

    if (form.is_valid and request.method == "POST"):
        form = NoteForm(request.POST)
        form.save()
        return HttpResponseRedirect('/lab_4')
    else:
        form.error_messages
    
    context = {'form':form}
    return render (request, 'lab4_form.html', context)

