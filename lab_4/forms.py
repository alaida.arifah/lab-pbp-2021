from django import forms
from lab_4.models import Note

class NoteForm(forms.ModelForm):
    class Meta:
        model = Note
        fields = '__all__'

    error_messages = {
        'required' : 'Please type'
    }

    to_person = forms.CharField(label="To", required=True, max_length=30, widget=forms.TextInput(attrs={'type':'text', 'placeholder':'To:'}))
    from_person = forms.CharField(label="From", required=True, max_length=30, widget=forms.TextInput(attrs={'type':'text', 'placeholder':'From:'}))
    title = forms.CharField(label="Title", required=True, max_length=30, widget=forms.TextInput(attrs={'type':'text', 'placeholder':'Title'}))
    message = forms.CharField(label="Message", required=True, max_length=1000, widget=forms.TextInput(attrs={'type':'text', 'placeholder':'Message'}))

