from django.db import models

# Create your models here.

class Note(models.Model):
    to_person = models.CharField(max_length = 30)
    from_person = models.CharField(max_length = 30)
    title = models.CharField(max_length = 30)
    message = models.CharField(max_length = 1000)