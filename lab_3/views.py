from django.contrib.auth.decorators import login_required
from django.http.response import HttpResponseRedirect
from django.shortcuts import render
from .models import Friend
from .forms import FriendForm

# Create your views here.

@login_required(login_url="/admin/login")
def index(request):
    friends = Friend.objects.all()  # TODO Implement this
    response = {'friends': friends}
    return render(request, 'lab3_index.html', response)

@login_required(login_url="/admin/login")
def add_friend(request):
  
    # create object of form
    form = FriendForm()
      
    # check if form data is valid
    if (form.is_valid and request.method == 'POST'):
        form = FriendForm(request.POST)
        form.save()
        return HttpResponseRedirect('/lab_3')
    else:
        form.error_messages

    context = {'form':form}
    return render(request, "lab3_form.html", context)
