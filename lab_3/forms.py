from django import forms
from lab_3.models import Friend
import datetime

class FriendForm(forms.ModelForm):
    class Meta:
        model = Friend
        fields = '__all__'
    error_messages = {
        'required' : 'Please type'
    }
    cur_year = datetime.datetime.today().year
    year_range = tuple([i for i in range(cur_year-100, cur_year+1)])
    name = forms.CharField(label='Nama', required=True, max_length=50, widget=forms.TextInput(attrs={'type':'text', 'placeholder':'Alaida'}))
    npm = forms.CharField(label='NPM', required=True, max_length=10, widget=forms.TextInput(attrs={'type':'text', 'placeholder':'2006597582'}))
    dob = forms.DateField(initial=datetime.date.today(), required=True, widget=forms.SelectDateWidget(years=year_range))